const fs = require("node:fs");
const files = fs.readdirSync(".");

const urls = files.map(file => {
  if (file.endsWith(".dfpwm") && file !== "") return "https://gitea.moe/Hri7566/cc-dfpwm/raw/branch/main/" + encodeURIComponent(file);
});

fs.writeFileSync("./urls.txt", urls.join("\n").replace(/^\s*$(?:\r\n|\n)/gm, ""));
